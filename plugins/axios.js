export default function ({ $axios, store, redirect, app }) {
  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    const errorMessage =
      error.response &&
      error.response.data &&
      error.response.data.errors &&
      error.response.data.errors.length > 0
        ? error.response.data.errors[0]
        : 'An Error Occured'
    if(app.$swal instanceof Function) {
      app.$swal({
        title: `error ${code} \n ${errorMessage}`,
        icon: 'error',
        position: 'top-right',
        toast: true,
        showConfirmButton: false,
        timer: 3000,
      })
    }
    if (code === 401) {
      app.$auth.$storage.setUniversal('auth_token', null, true)
      app.$auth.$storage.setUniversal('user', null, true)
      app.$auth.$setUser(null)
      redirect('/login')
    }
  })

  $axios.onResponse(() => {
    if (app.$swal instanceof Function) {
      app.$swal({
        title: `done`,
        icon: 'success',
        position: 'top-right',
        toast: true,
        showConfirmButton: false,
        timer: 2000,
      })
    }
  })

  $axios.onRequest((config) => {
    config.headers.common['Authorization'] = `Bearer ${store.state.login.access_token}`
  })
}
