import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
  ingredients: [],
  image: {
    img: null,
    type: 'recipe',
  },
})

export const getters = {
  getField,
  ingredients: (state) => state.ingredients,
  image: (state) => state.image,
}
export const mutations = {
  updateField,
  update_ingredients: (state, value) => (state.ingredients = value),
  update_image: (state, value) => (state.image.img = value),
}
export const actions = {
  async search_ingredients({}, { key }) {
    if (!key) return
    try {
      const response = await this.$axios.$get(
        `/recipe/ingredients?key_search=${key}`
      )
      return response.ingredients
    } catch (e) {
      console.log(e)
    }
  },

  async upload_image({ getters }, { image }) {
    const fomrdata = new FormData()
    _.forEach(image, (value, key) => {
      fomrdata.append(key, value)
    })

    for (var pair of fomrdata.entries()) {
      console.log(pair[0] + ', ' + pair[1])
    }

    try {
      const response = await this.$axios.$post('/image/', fomrdata)
      return response
    } catch (e) {
      throw e
    }
  },
}
