import { getField, updateField } from 'vuex-map-fields'

const defaultFilterItems = {
  include_ingredients: [],
  exclude_ingredients: [],
  main_ingredients: [],
  categories_ids: [],
  difficulty_level_ids: [],
  duration_ids: [],
  cost_ids: [],
}

export const state = () => ({
  filter: {
    ...defaultFilterItems,
  },
  pagination: {
    current_page: 1,
    per_page: 12,
    total: null,
  },
  recipes: [],
})

export const getters = {
  getField,
  filterItems: (state) => state.filterItems,
}
export const mutations = {
  updateField,
  update_ingredients: (state, value) => (state.ingredients = value),
  update_recipes: (state, value) => (state.recipes = value),
  update_current_page: (state, value) =>
    (state.pagination.current_page = value),
  update_per_page: (state, value) => (state.pagination.per_page = value),
  update_total: (state, value) => (state.pagination.total = value),
}
export const actions = {
  async search_ingredients({ commit }, { key }) {
    if (!key) return
    try {
      const response = await this.$axios.$get(
        `/recipe/ingredients?key_search=${key}`
      )
      commit('update_ingredients', response.recipes.ingredients)
    } catch (e) {
      console.log(e)
    }
  },
  async get_recipes({ state, commit }) {
    try {
      const recipes = await this.$axios.$get('/recipe', {
        params: {
          ...state.filter,
          page: state.pagination.current_page,
          per_page: state.pagination.per_page,
        },
      })
      commit('update_recipes', recipes.data)
      commit('update_current_page', recipes.current_page)
      commit('update_per_page', recipes.per_page)
      commit('update_total', recipes.total)
    } catch (e) {
      console.log(e)
    }
  },
}
