import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
  recipe: null
})

export const getters = {
  getField,
}
export const mutations = {
  updateField,
  update_recipe: (state, value) => (state.recipe = value),
}
export const actions = {
  async get_recipe({ commit }, { id }) {
    try {
      const recipe  = await this.$axios.$get(`/recipe/${id}`)
      commit('update_recipe', recipe)
    } catch (e) {
      console.log(e)
    }
  },
}
