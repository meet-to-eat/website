import { getField, updateField } from 'vuex-map-fields'
import { pick } from 'lodash'
import { P } from '~/static/js/libs/handlebars.runtime'
const ingredientObject = {
  ingredient_id: {
    id: -1,
    name: '',
  },
  unit_id: null,
  quantity: 0,
}
const stepObject = {
  content: null,
}

export const state = () => ({
  data: {
    name: null,
    main_img: null,
    description: null,
    tips: null,
    category_id: null,
    duration_id: null,
    difficulty_level_id: null,
    cost_id: null,
    ingredients: [ingredientObject],
    steps: [stepObject],
    tags: [],
    missing_ingredients: [],
  },
})

export const getters = {
  getField,
  form_data: (state, getters) => {
    const form_data = {
      ...state.data,
    }

    // override ingredients.ingredeint_id
    // currently if stores ingredient object
    // we need to check if its id == -1
    // then it's new ingredient and we should add its name to 'missing_ingredients' array
    // else (any other id)
    // then we should add its id as value of 'ingredients.ingredeint_id';

    form_data.ingredients = form_data.ingredients
      .map((ingredient) => {
        const id = ingredient.ingredient_id.id
        if (id > -1)
          return {
            ingredient_id: id,
            quantity: ingredient.quantity,
            unit_id: ingredient.unit_id,
          }
        else {
          return null
        }
      })
      .filter((item) => item !== null)
    form_data.missing_ingredients = getters.missing_ingredients
    return form_data
  },
  missing_ingredients: (state) =>
    state.data.ingredients
      .map((ingredient) => {
        if (ingredient.ingredient_id.id === -1)
          return {
            name: ingredient.ingredient_id.name,
            quantity: ingredient.quantity,
            unit_id: ingredient.unit_id,
          }
        else {
          return null
        }
      })
      .filter((item) => item !== null),
}
export const mutations = {
  updateField,
  add_ingredient: (state) =>
    state.data.ingredients.push({ ...ingredientObject }),
  add_step: (state) => state.data.steps.push({ ...stepObject }),
  add_tag: (state, value) => state.data.tags.push(value),
  update_main_img: (state, value) => (state.data.main_img = value),
  update_step_img: (state, { id, index }) =>
    (state.data.steps[index].img_id = id),
  remove_ingredient: (state, value) =>{
    console.log(value),
    state.data.ingredients.splice(value, 1)
  },
}
export const actions = {
  async store_recipe({ getters, commit }) {
    console.log(getters.form_data)
    try {
      return await this.$axios.$post('/recipe', {
        ...getters.form_data,
      })
    } catch (e) {
      throw e
    }
  },
}
