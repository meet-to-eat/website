import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
  data: {
    first_name: '',
    last_name: '',
    email: '',
    birthday: new Date().toISOString().substr(0, 10),
    mobile_number: '',
    international_dialing_code: '+963',
    password: '',
    password_confirmation: '',
    country_id: '',
    gender_id: 1,
  },

  // token
  access_token: null,
})

export const getters = {
  getField,
  data: (state) => state.data,
  loginFields: (state) => ({
    international_dialing_code: state.data.international_dialing_code,
    email: state.data.email,
    password: state.data.password,
  }),
}
export const mutations = {
  updateField,
  updateToken: function (state, { TOKEN }) {
    this.$auth.$storage.setUniversal('auth_token', TOKEN, true)
    state.access_token = TOKEN
  },
  updateUser: function (_, { user }) {
    this.$auth.setUser(user)
    this.$auth.$storage.setUniversal('user', user, true)
  },
}
export const actions = {
  async signup_submit({ state, commit }) {
    try {
      const response = await this.$axios.$post('/auth/signup', state.data)

      // save authorization token to nuxt universal storage
      const TOKEN = response.access_token
      commit('updateToken', { TOKEN })

      // set user to login using nuxt auth module & save user to nuxt universal storage
      commit('updateUser', { user: response.user })
    } catch (e) {
      console.log(e)
    }
  },

  async login_submit({ getters, commit }) {
    try {
      const response = await this.$axios.$post(
        '/auth/login',
        getters.loginFields
      )

      // save authorization token to nuxt universal storage
      const TOKEN = response.access_token
      commit('updateToken', { TOKEN })

      // set user to login using nuxt auth module & save user to nuxt universal storage
      commit('updateUser', { user: response.user })
    } catch (e) {
      console.log(e)
    }
  },
}
