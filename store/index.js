import { getField } from 'vuex-map-fields'

export const state = () => ({
  // filter Options
  categories: null,
  difficulties: null,
  costs: null,
  durations: null,
  units: null,

  // config options
  countries: null,
  genders: null,

})

export const getters = {
  getField,
  sub_categories: (state) => {
    const sub_categories = []
    state.categories.forEach((element) => {
      element.sub_categories.forEach((sub) => {
        sub_categories.push(sub)
      })
    })
    return sub_categories
  },
}

export const mutations = {
  // filter Options Mutation
  setCategories: (state, value) => (state.categories = value),
  setDifficulties: (state, value) => (state.difficulties = value),
  setCosts: (state, value) => (state.costs = value),
  setDurations: (state, value) => (state.durations = value),
  setUnits: (state, value) => (state.units = value),

  // Config Options Mutation
  setCountries: (state, value) => (state.countries = value),
  setGenders: (state, value) => (state.genders = value),
}
export const actions = {
  async nuxtServerInit({ dispatch, commit }) {
    await dispatch('init')

    // load user into vuex store
    const user = this.$auth.$storage.getUniversal('user', true)
    if (user) {
      this.$auth.setUser(user)
    }

    // load authorization hedaer into axios
    const auth_token = this.$auth.$storage.getUniversal('auth_token', true)
    if (auth_token) {
      commit('login/updateToken', {
        TOKEN: auth_token,
      })
    }
  },

  async init({ commit }) {
    // init api (loaded from server side)
    const data = await this.$axios.$get('/init')
    if (data) {
      // update filter options
      commit('setCategories', data.categories)
      commit('setDifficulties', data.difficulties)
      commit('setCosts', data.costs)
      commit('setDurations', data.durations)
      commit('setUnits', data.units)

      // update config options
      commit('setCountries', data.countries)
      commit('setGenders', data.genders)
    }
  },
}
