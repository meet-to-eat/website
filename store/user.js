import { getField, updateField } from 'vuex-map-fields'

export const state = () => ({
  interests_categories: [],
  user: null,
})

export const getters = {
  getField,
  categories: (state) => state.categories,
}
export const mutations = {
  updateField,
  update_user: (state, value) => (state.user = value),
  toggle_interests_category: (state, { item }) => {
    // check if category is selected before
    const itemIndex = state.interests_categories.findIndex(
      (el) => el === item.id
    )
    if (itemIndex > -1) {
      // category exists: so it'll be removed
      state.interests_categories.splice(itemIndex, 1)
    } else {
      // category not exists: so it'll be added
      state.interests_categories.push(item.id)
    }
  },
}
export const actions = {
  async update_interests_categories({ state }) {
    try {
      await this.$axios.$post('/profile/interestCategories', {
        categories: state.interests_categories,
      })
    } catch (e) {
      console.log(e)
    }
  },

  async get_user({ commit }, { id }) {
    try {
      const { chief } = await this.$axios.$get(`/chief/${id}`)
      console.log(chief)
      commit('update_user', chief)
    } catch (e) {
      console.log(e)
    }
  },
}
