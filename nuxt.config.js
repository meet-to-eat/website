// localization files
import webpack from 'webpack'
const en = require('./locales/en.json')
const ar = require('./locales/ar.json')

// urls
// const API_URL = 'http://192.168.1.104/MeetToEat/public/api'
const API_URL = 'http://192.168.43.151/MeetToEat/public/api'
const BASE_URL = 'http://localhost:3000'

export default {
	/*
	 ** Nuxt target
	 ** See https://nuxtjs.org/api/configuration-target
	 */
	target: 'server',
	/*
	 ** Headers of the page
	 ** See https://nuxtjs.org/api/configuration-head
	 */
	head: {
		titleTemplate: '%s - ' + process.env.npm_package_name,
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1',
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || '',
			},
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],

		// theme scripts
		script: [
			{ src: '/js/jQuery/jquery-3.4.1.js', body: true },
			{ src: '/js/libs/jquery.appear.js', body: true },
			{ src: '/js/libs/bootstrap.bundle.js', body: true },
			{ src: '/js/libs/jquery.mousewheel.js', body: true },
			{ src: '/js/libs/perfect-scrollbar.js', body: true },
			{ src: '/js/libs/jquery.matchHeight.js', body: true },
			{ src: '/js/libs/svgxuse.js', body: true },
			{ src: '/js/libs/imagesloaded.pkgd.js', body: true },
			{ src: '/js/libs/Headroom.js', body: true },
			{ src: '/js/libs/velocity.js', body: true },
			{ src: '/js/libs/ScrollMagic.js', body: true },
			{ src: '/js/libs/jquery.waypoints.js', body: true },
			{ src: '/js/libs/jquery.countTo.js', body: true },
			{ src: '/js/libs/popper.min.js', body: true },
			{ src: '/js/libs/material.min.js', body: true },
			{ src: '/js/libs/bootstrap-select.js', body: true },
			{ src: '/js/libs/smooth-scroll.js', body: true },
			{ src: '/js/libs/selectize.js', body: true },
			{ src: '/js/libs/swiper.jquery.js', body: true },
			{ src: '/js/libs/moment.js', body: true },
			{ src: '/js/libs/daterangepicker.js', body: true },
			{ src: '/js/libs/fullcalendar.js', body: true },
			{ src: '/js/libs/isotope.pkgd.js', body: true },
			{ src: '/js/libs/ajax-pagination.js', body: true },
			{ src: '/js/libs/Chart.js', body: true },
			{ src: '/js/libs/chartjs-plugin-deferred.js', body: true },
			{ src: '/js/libs/circle-progress.js', body: true },
			{ src: '/js/libs/loader.js', body: true },
			{ src: '/js/libs/run-chart.js', body: true },
			{ src: '/js/libs/jquery.magnific-popup.js', body: true },
			{ src: '/js/libs/jquery.gifplayer.js', body: true },
			{ src: '/js/libs/mediaelement-and-player.js', body: true },
			{ src: '/js/libs/mediaelement-playlist-plugin.min.js', body: true },
			{ src: '/js/libs/ion.rangeSlider.js', body: true },
			{ src: '/js/libs/leaflet.js', body: true },
			{ src: '/js/libs/MarkerClusterGroup.js', body: true },
			{ src: '/js/main.js', body: true },
			{ src: '/js/libs-init/libs-init.js', body: true },
		],
	},
	/*
	 ** Global CSS
	 */
	css: [
		// bootstrap
		'@/static/css/bootstrap-reboot.css',
		'@/static/css/bootstrap.css',
		'@/static/css/bootstrap-grid.css',
		'@/static/css/line-awesome.css',

		// theme assets
		'@/assets/theme/sass/main.scss',
	],
	/*
	 ** Plugins to load before mounting the App
	 ** https://nuxtjs.org/guide/plugins
	 */
	plugins: [
    '~/plugins/axios',
  ],
	/*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
	components: true,
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: ['@nuxtjs/vuetify'],
	/*
	 ** Nuxt.js modules
	 */
  router: {
    middleware: ['auth']
  },

	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/auth',
    'nuxt-i18n',
    '@nuxtjs/proxy',
    'vue-sweetalert2/nuxt'
	],
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/
	 */
	axios: {
    baseURL: API_URL,
    proxyHeaders: false,
    credentials: false
  },
  proxy: {
    '/api': API_URL
  },
	/*
	 ** Axios module configuration
	 ** See https://auth.nuxtjs.org/
	 */
	auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/auth/login', method: 'post' },
        },
      }
    }
  },
	/*
	 ** i18n module configuration
	 ** See https://i18n.nuxtjs.org
	 */
	i18n: {
		locales: ['en', 'ar'],
		defaultLocale: 'en',
		lazy: true,
		vueI18n: {
			fallbackLocale: 'en',
			messages: { en, ar },
		},
	},
	/*
	 ** vuetify module configuration
	 ** https://github.com/nuxt-community/vuetify-module
	 */
	vuetify: {
		customVariables: ['@/assets/variables.scss'],
		treeShake: true,
		options: {
			customProperties: true,
		},
		theme: {
			themes: {
				light: {
					primary: '#E63946',
					secondary: '#1D3557',
					accent: '#457B9D',
				},
			},
		},
		breakpoint: {
			thresholds: {
				xs: 340,
				sm: 576,
				md: 768,
				lg: 992,
				xl: 1200,
			},
		},
	},
	/*
	 ** Build configuration
	 ** See https://nuxtjs.org/api/configuration-build/
	 */
	build: {
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        _: 'lodash'
      })
    ]
  },
}
